<?php

/**
 * Description of Collection
 *
 * @author david
 */
class Osdave_LanguageCsv_Model_File_Collection extends Varien_Data_Collection_Filesystem
{

    /** @var string*/
    protected $_sizeLabel;

    /** @var bool  */
    protected $_isUnix = false;

    /**
     * Folder, where all language files are stored are stored
     *
     * @var string
     */
    protected $_baseDir;

    /**
     * Set collection specific parameters and make sure language files folder will exist
     */
    public function __construct()
    {
        parent::__construct();
        $this->_detectOS();

        $this->_baseDir = Mage::getBaseDir('var') . DS . 'languagecsv';

        // check for valid base dir
        $ioProxy = new Varien_Io_File();
        $ioProxy->mkdir($this->_baseDir);
        if (!is_file($this->_baseDir . DS . '.htaccess')) {
            $ioProxy->open(array('path' => $this->_baseDir));
            $ioProxy->write('.htaccess', 'deny from all', 0644);
        }

        // set collection specific params
        $this
            ->setOrder('name', self::SORT_ORDER_ASC)
            ->addTargetDir($this->_baseDir)
            ->setFilesFilter('/^[a-zA-Z0-9\-\_]+\.' . preg_quote(Osdave_LanguageCsv_Model_File::LANGUAGE_FILE_EXTENSION, '/') . '$/')
            ->setCollectRecursively(false);
    }

    /**
     * If OS is *nix then number of lines will be used as the 'size' attribute
     *
     * @return $this
     */
    protected function _detectOS()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->_sizeLabel = Mage::helper('languagecsv')->__('File Size');
        } else {
            $this->_sizeLabel = Mage::helper('languagecsv')->__('Number of Lines');
            $this->_isUnix = true;
        }
        return $this;
    }

    /**
     * _sizeLabel getter
     *
     * @return string
     */
    public function getSizeLabel()
    {
        return $this->_sizeLabel;
    }

    /**
     * Get language files-specific data from model for each row
     *
     * @param string $filename
     * @return array
     */
    protected function _generateRow($filename)
    {
        $row = parent::_generateRow($filename);
        foreach (Mage::getSingleton('languagecsv/file')->load($row['basename'], $this->_baseDir)
                     ->getData() as $key => $value) {
            $row[$key] = $value;
        }

        if ($this->_isUnix) {
            $row['size'] = intval(exec("wc -l '$filename'"));
        } else {
            $row['size'] = filesize($filename);
        }

        return $row;
    }
}